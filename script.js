// [name, age, stasus, income] - это заголовок для таблицы
var arr = [
    {
          name: 'Vasya',
          age: 20,
          status: 'single',
          income: '5000$'
    },
    {
          name: 'Olya',
          age: 30,
          status: 'married',
          income: '10000$'
    },
    {
          name: 'Ihor',
          age: 40,
          status: 'married',
          income: '8000$'
     },
    {
          name: 'Anna',
          age: 19,
          status: 'single',
          income: '1000$'
     }
];


var table = document.createElement('table');
var tr = document.createElement('tr');

for(i in arr[0]){
  var td = document.createElement('td');
  td.innerText = i;
  tr.appendChild(td);
};
table.appendChild(tr);


for (i in arr){
  var tr = document.createElement('tr');
  for (j in arr[i]){
    var td = document.createElement('td')
    td.innerText = arr[i][j];
    tr.appendChild(td);
  }
  table.appendChild(tr);
};

document.body.appendChild(table);